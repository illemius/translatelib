"""
Generated from: http://www.science.co.il/Language/Locale-codes.asp
"""


# TODO: Documentation
# TODO: Samples
# TODO: Change old samples with new feature


class LocaleDetail:
    def __init__(self, locale=None, code=None, string=None, decimal=None, hexadecimal=None, codepage=None):
        self._locale = locale
        self._code = code
        self._lcid_string = string
        self._lcid_dec = decimal
        self._lcid_hex = hexadecimal
        self._code_page = codepage

    def check(self, locale):
        """
        Check query and LCID_String
        :param locale:
        :return:
        """
        query = locale.lower().strip().replace('_', '-')
        if query == self._lcid_string:
            return True
        return False

    def get_name(self):
        """
        Language name
        :return:
        """
        return self._locale

    def get_short_name(self):
        """
        Short language name
        :return:
        """
        return self._locale.split('-')[0].strip()

    def get_lang_code(self):
        """
        Get language code
        :return:
        """
        return self._code

    def get_lcid_string(self):
        """
        Get string LCID
        :return:
        """
        return self._lcid_string

    def get_lcid_decimal(self):
        """
        Get decimal LCID
        :return:
        """
        return self._lcid_dec

    def get_lcid_hex(self):
        """
        Get hexadecimal LCID
        :return:
        """
        return self._lcid_hex

    def get_code_page(self):
        """
        Get codepage
        :return:
        """
        return self._code_page


locales_info = (
    LocaleDetail('Afrikaans', 'af', 'af', 1078, 436, 1252),
    LocaleDetail('Albanian', 'sq', 'sq', 1052, None, 1250),
    LocaleDetail('Amharic', 'am', 'am', 1118),
    LocaleDetail('Arabic - Algeria', 'ar', 'ar-dz', 5121, 1401, 1256),
    LocaleDetail('Arabic - Bahrain', 'ar', 'ar-bh', 15361, None, 1256),
    LocaleDetail('Arabic - Egypt', 'ar', 'ar-eg', 3073, None, 1256),
    LocaleDetail('Arabic - Iraq', 'ar', 'ar-iq', 2049, 801, 1256),
    LocaleDetail('Arabic - Jordan', 'ar', 'ar-jo', 11265, None, 1256),
    LocaleDetail('Arabic - Kuwait', 'ar', 'ar-kw', 13313, 3401, 1256),
    LocaleDetail('Arabic - Lebanon', 'ar', 'ar-lb', 12289, 3001, 1256),
    LocaleDetail('Arabic - Libya', 'ar', 'ar-ly', 4097, 1001, 1256),
    LocaleDetail('Arabic - Morocco', 'ar', 'ar-ma', 6145, 1801, 1256),
    LocaleDetail('Arabic - Oman', 'ar', 'ar-om', 8193, 2001, 1256),
    LocaleDetail('Arabic - Qatar', 'ar', 'ar-qa', 16385, 4001, 1256),
    LocaleDetail('Arabic - Saudi Arabia', 'ar', 'ar-sa', 1025, 401, 1256),
    LocaleDetail('Arabic - Syria', 'ar', 'ar-sy', 10241, 2801, 1256),
    LocaleDetail('Arabic - Tunisia', 'ar', 'ar-tn', 7169, None, 1256),
    LocaleDetail('Arabic - United Arab Emirates', 'ar', 'ar-ae', 14337, 3801, 1256),
    LocaleDetail('Arabic - Yemen', 'ar', 'ar-ye', 9217, 2401, 1256),
    LocaleDetail('Armenian', 'hy', 'hy', 1067),
    LocaleDetail('Assamese', 'as', 'as', 1101),
    LocaleDetail('Azeri - Cyrillic', 'az', 'az-az', 2092, None, 1251),
    LocaleDetail('Azeri - Latin', 'az', 'az-az', 1068, None, 1254),
    LocaleDetail('Basque', 'eu', 'eu', 1069, None, 1252),
    LocaleDetail('Belarusian', 'be', 'be', 1059, 423, 1251),
    LocaleDetail('Bengali - Bangladesh', 'bn', 'bn', 2117, 845),
    LocaleDetail('Bengali - India', 'bn', 'bn', 1093, 445),
    LocaleDetail('Bosnian', 'bs', 'bs', 5146),
    LocaleDetail('Bulgarian', 'bg', 'bg', 1026, 402, 1251),
    LocaleDetail('Burmese', 'my', 'my', 1109, 455),
    LocaleDetail('Catalan', 'ca', 'ca', 1027, 403, 1252),
    LocaleDetail('Chinese - China', 'zh', 'zh-cn', 2052, 804),
    LocaleDetail('Chinese - Hong Kong SAR', 'zh', 'zh-hk', 3076),
    LocaleDetail('Chinese - Macau SAR', 'zh', 'zh-mo', 5124, 1404),
    LocaleDetail('Chinese - Singapore', 'zh', 'zh-sg', 4100, 1004),
    LocaleDetail('Chinese - Taiwan', 'zh', 'zh-tw', 1028, 404),
    LocaleDetail('Croatian', 'hr', 'hr', 1050, None, 1250),
    LocaleDetail('Czech', 'cs', 'cs', 1029, 405, 1250),
    LocaleDetail('Danish', 'da', 'da', 1030, 406, 1252),
    LocaleDetail('Divehi; Dhivehi; Maldivian', 'dv', 'dv', 1125, 465),
    LocaleDetail('Dutch - Belgium', 'nl', 'nl-be', 2067, 813, 1252),
    LocaleDetail('Dutch - Netherlands', 'nl', 'nl-nl', 1043, 413, 1252),
    LocaleDetail('Edo', None, None, 1126, 466),
    LocaleDetail('English - Australia', 'en', 'en-au', 3081, None, 1252),
    LocaleDetail('English - Belize', 'en', 'en-bz', 10249, 2809, 1252),
    LocaleDetail('English - Canada', 'en', 'en-ca', 4105, 1009, 1252),
    LocaleDetail('English - Caribbean', 'en', 'en-cb', 9225, 2409, 1252),
    LocaleDetail('English - Great Britain', 'en', 'en-gb', 2057, 809, 1252),
    LocaleDetail('English - India', 'en', 'en-in', 16393, 4009),
    LocaleDetail('English - Ireland', 'en', 'en-ie', 6153, 1809, 1252),
    LocaleDetail('English - Jamaica', 'en', 'en-jm', 8201, 2009, 1252),
    LocaleDetail('English - New Zealand', 'en', 'en-nz', 5129, 1409, 1252),
    LocaleDetail('English - Phillippines', 'en', 'en-ph', 13321, 3409, 1252),
    LocaleDetail('English - Southern Africa', 'en', 'en-za', 7177, None, 1252),
    LocaleDetail('English - Trinidad', 'en', 'en-tt', 11273, None, 1252),
    LocaleDetail('English - United States', 'en', 'en-us', 1033, 409, 1252),
    LocaleDetail('English - Zimbabwe', 'en', None, 12297, 3009, 1252),
    LocaleDetail('Estonian', 'et', 'et', 1061, 425, 1257),
    LocaleDetail('Faroese', 'fo', 'fo', 1080, 438, 1252),
    LocaleDetail('Farsi - Persian', 'fa', 'fa', 1065, 429, 1256),
    LocaleDetail('Filipino', None, None, 1124, 464),
    LocaleDetail('Finnish', 'fi', 'fi', 1035, None, 1252),
    LocaleDetail('French - Belgium', 'fr', 'fr-be', 2060, None, 1252),
    LocaleDetail('French - Cameroon', 'fr', None, 11276),
    LocaleDetail('French - Canada', 'fr', 'fr-ca', 3084, None, 1252),
    LocaleDetail('French - Congo', 'fr', None, 9228),
    LocaleDetail('French - Cote d\'Ivoire', 'fr', None, 12300),
    LocaleDetail('French - France', 'fr', 'fr-fr', 1036, None, 1252),
    LocaleDetail('French - Luxembourg', 'fr', 'fr-lu', 5132, None, 1252),
    LocaleDetail('French - Mali', 'fr', None, 13324),
    LocaleDetail('French - Monaco', 'fr', None, 6156, None, 1252),
    LocaleDetail('French - Morocco', 'fr', None, 14348),
    LocaleDetail('French - Senegal', 'fr', None, 10252),
    LocaleDetail('French - Switzerland', 'fr', 'fr-ch', 4108, None, 1252),
    LocaleDetail('French - West Indies', 'fr', None, 7180),
    LocaleDetail('Frisian - Netherlands', None, None, 1122, 462),
    LocaleDetail('FYRO Macedonia', 'mk', 'mk', 1071, None, 1251),
    LocaleDetail('Gaelic - Ireland', 'gd', 'gd-ie', 2108),
    LocaleDetail('Gaelic - Scotland', 'gd', 'gd', 1084),
    LocaleDetail('Galician', 'gl', None, 1110, 456, 1252),
    LocaleDetail('Georgian', 'ka', None, 1079, 437),
    LocaleDetail('German - Austria', 'de', 'de-at', 3079, None, 1252),
    LocaleDetail('German - Germany', 'de', 'de-de', 1031, 407, 1252),
    LocaleDetail('German - Liechtenstein', 'de', 'de-li', 5127, 1407, 1252),
    LocaleDetail('German - Luxembourg', 'de', 'de-lu', 4103, 1007, 1252),
    LocaleDetail('German - Switzerland', 'de', 'de-ch', 2055, 807, 1252),
    LocaleDetail('Greek', 'el', 'el', 1032, 408, 1253),
    LocaleDetail('Guarani - Paraguay', 'gn', 'gn', 1140, 474),
    LocaleDetail('Gujarati', 'gu', 'gu', 1095, 447),
    LocaleDetail('Hebrew', 'he', 'he', 1037, None, 1255),
    LocaleDetail('HID (Human Interface Device)', None, None, 1279),
    LocaleDetail('Hindi', 'hi', 'hi', 1081, 439),
    LocaleDetail('Hungarian', 'hu', 'hu', 1038, None, 1250),
    LocaleDetail('Icelandic', 'is', 'is', 1039, None, 1252),
    LocaleDetail('Igbo - Nigeria', None, None, 1136, 470),
    LocaleDetail('Indonesian', 'id', 'id', 1057, 421, 1252),
    LocaleDetail('Italian - Italy', 'it', 'it-it', 1040, 410, 1252),
    LocaleDetail('Italian - Switzerland', 'it', 'it-ch', 2064, 810, 1252),
    LocaleDetail('Japanese', 'ja', 'ja', 1041, 411),
    LocaleDetail('Kannada', 'kn', 'kn', 1099),
    LocaleDetail('Kashmiri', 'ks', 'ks', 1120, 460),
    LocaleDetail('Kazakh', 'kk', 'kk', 1087, None, 1251),
    LocaleDetail('Khmer', 'km', 'km', 1107, 453),
    LocaleDetail('Konkani', None, None, 1111, 457),
    LocaleDetail('Korean', 'ko', 'ko', 1042, 412),
    LocaleDetail('Kyrgyz - Cyrillic', None, None, 1088, 440, 1251),
    LocaleDetail('Lao', 'lo', 'lo', 1108, 454),
    LocaleDetail('Latin', 'la', 'la', 1142, 476),
    LocaleDetail('Latvian', 'lv', 'lv', 1062, 426, 1257),
    LocaleDetail('Lithuanian', 'lt', 'lt', 1063, 427, 1257),
    LocaleDetail('Malay - Brunei', 'ms', 'ms-bn', 2110, None, 1252),
    LocaleDetail('Malay - Malaysia', 'ms', 'ms-my', 1086, None, 1252),
    LocaleDetail('Malayalam', 'ml', 'ml', 1100),
    LocaleDetail('Maltese', 'mt', 'mt', 1082),
    LocaleDetail('Manipuri', None, None, 1112, 458),
    LocaleDetail('Maori', 'mi', 'mi', 1153, 481),
    LocaleDetail('Marathi', 'mr', 'mr', 1102),
    LocaleDetail('Mongolian', 'mn', 'mn', 2128, 850),
    LocaleDetail('Mongolian', 'mn', 'mn', 1104, 450, 1251),
    LocaleDetail('Nepali', 'ne', 'ne', 1121, 461),
    LocaleDetail('Norwegian - Bokml', 'nb', 'no-no', 1044, 414, 1252),
    LocaleDetail('Norwegian - Nynorsk', 'nn', 'no-no', 2068, 814, 1252),
    LocaleDetail('Oriya', 'or', 'or', 1096, 448),
    LocaleDetail('Polish', 'pl', 'pl', 1045, 415, 1250),
    LocaleDetail('Portuguese - Brazil', 'pt', 'pt-br', 1046, 416, 1252),
    LocaleDetail('Portuguese - Portugal', 'pt', 'pt-pt', 2070, 816, 1252),
    LocaleDetail('Punjabi', 'pa', 'pa', 1094, 446),
    LocaleDetail('Raeto-Romance', 'rm', 'rm', 1047, 417),
    LocaleDetail('Romanian - Moldova', 'ro', 'ro-mo', 2072, 818),
    LocaleDetail('Romanian - Romania', 'ro', 'ro', 1048, 418, 1250),
    LocaleDetail('Russian', 'ru', 'ru', 1049, 419, 1251),
    LocaleDetail('Russian - Moldova', 'ru', 'ru-mo', 2073, 819),
    LocaleDetail('Sami Lappish', None, None, 1083),
    LocaleDetail('Sanskrit', 'sa', 'sa', 1103),
    LocaleDetail('Serbian - Cyrillic', 'sr', 'sr-sp', 3098, None, 1251),
    LocaleDetail('Serbian - Latin', 'sr', 'sr-sp', 2074, None, 1250),
    LocaleDetail('Sesotho (Sutu)', None, None, 1072, 430),
    LocaleDetail('Setsuana', 'tn', 'tn', 1074, 432),
    LocaleDetail('Sindhi', 'sd', 'sd', 1113, 459),
    LocaleDetail('Sinhala; Sinhalese', 'si', 'si', 1115),
    LocaleDetail('Slovak', 'sk', 'sk', 1051, None, 1250),
    LocaleDetail('Slovenian', 'sl', 'sl', 1060, 424, 1250),
    LocaleDetail('Somali', 'so', 'so', 1143, 477),
    LocaleDetail('Sorbian', 'sb', 'sb', 1070),
    LocaleDetail('Spanish - Argentina', 'es', 'es-ar', 11274, None, 1252),
    LocaleDetail('Spanish - Bolivia', 'es', 'es-bo', 16394, None, 1252),
    LocaleDetail('Spanish - Chile', 'es', 'es-cl', 13322, None, 1252),
    LocaleDetail('Spanish - Colombia', 'es', 'es-co', 9226, None, 1252),
    LocaleDetail('Spanish - Costa Rica', 'es', 'es-cr', 5130, None, 1252),
    LocaleDetail('Spanish - Dominican Republic', 'es', 'es-do', 7178, None, 1252),
    LocaleDetail('Spanish - Ecuador', 'es', 'es-ec', 12298, None, 1252),
    LocaleDetail('Spanish - El Salvador', 'es', 'es-sv', 17418, None, 1252),
    LocaleDetail('Spanish - Guatemala', 'es', 'es-gt', 4106, None, 1252),
    LocaleDetail('Spanish - Honduras', 'es', 'es-hn', 18442, None, 1252),
    LocaleDetail('Spanish - Mexico', 'es', 'es-mx', 2058, None, 1252),
    LocaleDetail('Spanish - Nicaragua', 'es', 'es-ni', 19466, None, 1252),
    LocaleDetail('Spanish - Panama', 'es', 'es-pa', 6154, None, 1252),
    LocaleDetail('Spanish - Paraguay', 'es', 'es-py', 15370, None, 1252),
    LocaleDetail('Spanish - Peru', 'es', 'es-pe', 10250, None, 1252),
    LocaleDetail('Spanish - Puerto Rico', 'es', 'es-pr', 20490, None, 1252),
    LocaleDetail('Spanish - Spain (Traditional)', 'es', 'es-es', 1034, None, 1252),
    LocaleDetail('Spanish - Uruguay', 'es', 'es-uy', 14346, None, 1252),
    LocaleDetail('Spanish - Venezuela', 'es', 'es-ve', 8202, None, 1252),
    LocaleDetail('Swahili', 'sw', 'sw', 1089, 441, 1252),
    LocaleDetail('Swedish - Finland', 'sv', 'sv-fi', 2077, None, 1252),
    LocaleDetail('Swedish - Sweden', 'sv', 'sv-se', 1053, None, 1252),
    LocaleDetail('Syriac', None, None, 1114),
    LocaleDetail('Tajik', 'tg', 'tg', 1064, 428),
    LocaleDetail('Tamil', 'ta', 'ta', 1097, 449),
    LocaleDetail('Tatar', 'tt', 'tt', 1092, 444, 1251),
    LocaleDetail('Telugu', 'te', 'te', 1098),
    LocaleDetail('Thai', 'th', 'th', 1054),
    LocaleDetail('Tibetan', 'bo', 'bo', 1105, 451),
    LocaleDetail('Tsonga', 'ts', 'ts', 1073, 431),
    LocaleDetail('Turkish', 'tr', 'tr', 1055, None, 1254),
    LocaleDetail('Turkmen', 'tk', 'tk', 1090, 442),
    LocaleDetail('Ukrainian', 'uk', 'uk', 1058, 422, 1251),
    LocaleDetail('Unicode', None, 'UTF-8', 0),
    LocaleDetail('Urdu', 'ur', 'ur', 1056, 420, 1256),
    LocaleDetail('Uzbek - Cyrillic', 'uz', 'uz-uz', 2115, 843, 1251),
    LocaleDetail('Uzbek - Latin', 'uz', 'uz-uz', 1091, 443, 1254),
    LocaleDetail('Venda', None, None, 1075, 433),
    LocaleDetail('Vietnamese', 'vi', 'vi', 1066, None, 1258),
    LocaleDetail('Welsh', 'cy', 'cy', 1106, 452),
    LocaleDetail('Xhosa', 'xh', 'xh', 1076, 434),
    LocaleDetail('Yiddish', 'yi', 'yi', 1085),
    LocaleDetail('Zulu', 'zu', 'zu', 1077, 435),
)


def get_locale(name):
    for localeDetail in locales_info:
        if localeDetail.check(name):
            return localeDetail
    raise NameError('Unknown locale "%s"!' % name)
