def bool_to_str(boolean: bool, if_true='true', if_false='false') -> str:
    """
    Convert bool to string

    Or you can use callable objects in `if_true` & `if_false` params.
    Example:
        >>> def func_if_true(param):pass
        >>> def func_if_false(param):pass
        >>> func = bool_to_str(status, if_true=func_if_true, if_false=func_if_false)
        >>> func(user)

    :param boolean:
    :param if_true: value if True
    :param if_false: value if False
    :return:
    """
    return [if_false, if_true][bool(boolean)]


# Alias
bstr = bool_to_str
