from TranslateLib import get_num_ending


class BaseParser:
    """
    Skeleton for parsers
    Tag syntax: <COMMAND::*args>
    """
    # Need override
    command = ''

    def generate_result(self, params, **settings):
        """
        Need to override this method
        :param params:
        :param settings:
        :return:
        """
        raise NotImplementedError

    def check(self, tag):
        """
        Validate tag
        :param tag:
        :return:
        """
        assert len(tag) == 3
        return self.command == tag[1]

    def parse(self, text, tag, **settings):
        """
        Parse (replace)
        :param text:
        :param tag:
        :param settings:
        :return:
        """
        return text.replace(tag[0], self.generate_result(tag[2], **settings))


class GetNumEndings(BaseParser):
    """
    Syntax: <NUM::zero::one::four>
    """
    command = 'NUM'

    def generate_result(self, params, **settings):
        return get_num_ending(settings.get('number', 0), params)


class RandomNumber(BaseParser):
    """
    Get random number
    Syntax: <RAND_INT>
    """
    command = 'RAND_INT'

    def generate_result(self, params, **settings):
        from random import randint
        return str(randint(settings.get('random_from', 0), settings.get('random_to', 100)))
