import json
import os
import random

from TranslateLib.translate_parsers import BaseParser

DEFAULT_LOCALE_NAME = 'default'

FIRST_TAG_SYMBOL = '<'
LAST_TAG_SYMBOL = '>'
ESCAPE_SYMBOL = '\\'

locales = {}
global_locale = DEFAULT_LOCALE_NAME

parsers = []


def register_parser(parser):
    assert isinstance(parser, BaseParser)
    parsers.append(parser)


def locale_gen(path):
    """
    Generate locales cache
    Access only JSON files with dict
    :param path:
    :return:
    """
    # Check locales path
    if not os.path.isdir(path):
        raise FileNotFoundError('Not fount locales path! (%s)' % path)

    global locales, global_locale
    locales = {}

    # Generate locales cache
    for file in os.listdir(path):
        if not file.endswith('.json'):
            continue
        title = file[0:-5]

        if global_locale is None:
            global_locale = title

        with open(os.path.join(path, file), 'r', encoding='utf8') as f:
            try:
                locale = json.load(f)
            except:
                raise TypeError('Locale file "%s" is wrong!' % file)
            else:
                if type(locale) is dict:
                    locales[title] = locale
                else:
                    raise TypeError('Locales file content type must be a dict!')
    return get_locales_list()


def set_locale(locale):
    """
    Set default locale
    :param locale:
    :return:
    """
    if locale in locales:
        global global_locale
        global_locale = locale
    else:
        raise ValueError('Locale %s not found!' % locale)


def raw_translate(text: str, variant=None, locale=None, disable_parser=False) -> str:
    """
    Translate text

    If translate is list -> return random choice or variant number
    If translate is dict -> return chosen variant or first element

    Example:
        >>> from TranslateLib import locale_gen, translate as _
        >>> locale_gen()
        ["ru_RU", "ua_UA"] # For e.g.
        >>> print(_('some text'))
        Какой то текст

    See more examples in examples/translate_demo.py

    :param text: key
    :param variant: chosen variant for dict or list translate
    :param disable_parser: disable dict/list translate parser
    :param locale: locale
    :return:
    """
    if not bool(locale) and global_locale == DEFAULT_LOCALE_NAME:
        return text
    result = get_locale(locale).get(text, variant or text)
    if not disable_parser:
        if type(result) is list:
            if variant is None:
                return random.choice(result)
            var = abs(variant)
            if var > len(result):
                var = len(result) - 1
            return result[var]
        elif type(result) is dict and variant is not None:
            return result.get(str(variant), variant)
        elif type(result) is dict:
            return list(result.values())[0]
    return result


def get_raw_tags(text):
    start_pos = -1
    escape = -1
    for pos, symbol in enumerate(text):
        # Enable escaping
        if symbol == ESCAPE_SYMBOL and escape < 0:
            escape = pos

        # Enter symbol
        if symbol == FIRST_TAG_SYMBOL and escape < 0:
            start_pos = pos
            continue

        # Finish symbol - return result
        if symbol == LAST_TAG_SYMBOL and escape < 0:
            yield text[start_pos:pos + 1]
            start_pos = -1

        if escape < pos:
            escape = -1


def parse_tags(text):
    for element in get_raw_tags(text):
        data = element[1:-1].split('::')
        if len(data) >= 1:
            tag = data[0]
            args = [item.strip() for item in data[1:]]
            yield element, tag, args


def translate(text: str, variant=None, locale=None, disable_parser=False, **settings) -> str:
    """
    Read docstring from raw_translate function
    :param text:
    :param variant:
    :param locale:
    :param disable_parser:
    :param settings:
    :return:
    """
    text = raw_translate(text, variant, locale, disable_parser)
    for tag in parse_tags(text):
        for parser in parsers:
            if parser.check(tag):
                text = parser.parse(text, tag, **settings)
    return text


def get_locales_list() -> list:
    """
    Get list of locale names
    :return:
    """
    return tuple(locales.keys())


def get_locale(locale=None) -> dict:
    """
    Get locale from cache
    :param locale: locale name
    :return:
    """
    _locale = locale or global_locale
    if len(locales) == 0:
        raise Exception('Locales not generated!')
    if not bool(_locale):
        raise ValueError('Locale not set!')
    return locales[_locale]
