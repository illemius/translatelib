from distutils.core import setup

setup(
    name='TranslateLib',
    version='0.1.5',
    packages=['TranslateLib'],
    url='https://bitbucket.org/illemius/translatelib',
    license='MIT',
    author='Alex Root Junior',
    author_email='jroot.junior@gmail.com',
    description='Translate library'
)
