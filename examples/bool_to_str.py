from random import choice

from TranslateLib import bool_to_str

users = ['Teddy', 'Sid', 'Eda', 'Armandina', 'Maren', 'Odis', 'Harmony', 'Toney', 'Mitch', 'Erna']


def get_user_status(username):
    # pseudo code
    return choice([False, True])


for user in users:
    print('User "{user}" is {status}'.format(
        user=user,
        status=bool_to_str(get_user_status(user), 'Online', 'Offline')
    ))
