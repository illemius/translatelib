from TranslateLib import get_num_ending

MIN, MAX = 0, 100
for number in range(MIN, MAX + 1):
    print('{num} - {ru} - {en}'.format(
        num=number,
        ru=get_num_ending(number, ('яблоко', 'яблока', 'яблок')),
        en=get_num_ending(number, ('apple', 'apples'))
    ))
