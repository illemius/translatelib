import os
import sys

from TranslateLib import locale_gen, translate as _

PATH = os.path.dirname(os.path.realpath(sys.argv[0]))

# Define locales path
LOCALES_PATH = os.path.join(PATH, 'locales')

# Load locales. (return list of founded locales)
locales_list = locale_gen(LOCALES_PATH)

print('Found locales: "%s"' % '", "'.join(locales_list))

print('\nDemonstrate default translate:')
keys = ['test', 'test 2', 'undefined', 'defined']
for item in keys:
    print('\tOriginal: "%s"' % item)
    for locale in locales_list:
        translate = _(item, locale=locale)
        print('\t\t> "%s":\t"%s"' % (locale, translate))

print('\nDemonstrate random and listed translate variant:')
for locale in locales_list:
    print('\t> "%s":\trandom: "%s", choose 1: "%s"' \
          % (locale, _('random', locale=locale), _('random', variant=1, locale=locale)))

print('\nDemonstrate choice:')
for locale in locales_list:
    print('\t> "%s":\tnone = "%s", male = "%s", female = "%s", undefined = "%s"' \
          % (locale,
             _('sex', variant=0, locale=locale),
             _('sex', variant=1, locale=locale),
             _('sex', variant=2, locale=locale),
             _('sex', variant=3, locale=locale)
             ))

print('\tUse choice translate without variant and use default locale:')
print('\t\t> sex:', _('sex'))
