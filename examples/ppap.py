"""
Comic demonstration of `get_num_ending` function
(Pen-Pineapple-Apple-Pen)
https://www.youtube.com/watch?v=d9TpRfDdyU0
"""
from TranslateLib import get_num_ending

for number in range(10):
    print('{num} {pen}-{pineapple}-{apple}-{pen}'.format(
        num=number,
        pen=get_num_ending(number, ('ручка', 'ручки', 'ручек')),
        pineapple=get_num_ending(number, ('ананас', 'ананаса', 'ананасов')),
        apple=get_num_ending(number, ('яблоко', 'яблока', 'яблок'))
    ))

"""
Results:
0 ручек-ананасов-яблок-ручек
1 ручка-ананас-яблоко-ручка
2 ручки-ананаса-яблока-ручки
3 ручки-ананаса-яблока-ручки
4 ручки-ананаса-яблока-ручки
5 ручек-ананасов-яблок-ручек
6 ручек-ананасов-яблок-ручек
7 ручек-ананасов-яблок-ручек
8 ручек-ананасов-яблок-ручек
9 ручек-ананасов-яблок-ручек
"""
