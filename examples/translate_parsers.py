import os
import sys

from TranslateLib import locale_gen, register_parser, translate as _
from TranslateLib.translate_parsers import GetNumEndings, RandomNumber

PATH = os.path.dirname(os.path.realpath(sys.argv[0]))

# Define locales path
LOCALES_PATH = os.path.join(PATH, 'locales_parser')

# Load locales. (return list of founded locales)
locales_list = locale_gen(LOCALES_PATH)

# Register parser
register_parser(GetNumEndings())
register_parser(RandomNumber())

for number in range(5):
    for locale in locales_list:
        print(locale, _('apple is here', locale=locale, number=number).format(count=number))

for locale in locales_list:
    print(locale, _('random', locale=locale, random_from=0, random_to=100))

"""
Sample output:
    ru_RU тут 0 яблок
    en_US 0 apples is here
    ru_RU тут 1 яблоко
    en_US 1 apple is here
    ru_RU тут 2 яблока
    en_US 2 apples is here
    ru_RU тут 3 яблока
    en_US 3 apples is here
    ru_RU тут 4 яблока
    en_US 4 apples is here
    en_US Random number: 42
    ru_RU Случайное число: 64
"""
